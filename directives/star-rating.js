// shared scope directive
// angular
//   .module('nike')
//   .directive('starRating1', function() {
//     // DDO (directive definition object)
//     return {
//       restrict: 'E',
//       templateUrl: '/templates/directives/star-rating.html',
//       link: function(scope) {
//         function calculateStars(rating) {
//           scope.stars = [];
//
//           for (var i = 1; i <= 5; i++) {
//             if (i <= rating) { // filled star
//               scope.stars.push({
//                 value: i,
//                 filled: true
//               });
//             } else { // empty star
//               scope.stars.push({
//                 value: i,
//                 empty: true
//               });
//             }
//           }
//
//           console.log(scope.stars);
//         }
//
//         calculateStars(scope.vm.product.rating)
//       }
//     };
//   });

// angular
//   .module('nike')
//   .directive('starRating2', function() {
//     return {
//       restrict: 'E',
//       templateUrl: '/templates/directives/star-rating.html',
//       // isolate scope
//       scope: {
//         rating: '='
//       },
//       link: function(scope) {
//         function calculateStars(rating) {
//           scope.stars = [];
//
//           for (var i = 1; i <= 5; i++) {
//             if (i <= rating) { // filled star
//               scope.stars.push({
//                 value: i,
//                 filled: true
//               });
//             } else { // empty star
//               scope.stars.push({
//                 value: i,
//                 empty: true
//               });
//             }
//           }
//
//           console.log(scope.stars);
//         }
//
//         calculateStars(scope.rating);
//
//         scope.rate = function(value) {
//           console.log(value);
//           calculateStars(value);
//           scope.rating = value;
//         };
//       }
//     };
//   });


angular
  .module('nike')
  .directive('starRating3', function() {
    return {
      restrict: 'E',
      templateUrl: '/templates/directives/star-rating.html',
      // isolate scope
      scope: {
        rating: '=',
        action: '&'
      },
      link: function(scope) {
        function calculateStars(rating) {
          scope.stars = [];

          for (var i = 1; i <= 5; i++) {
            if (i <= rating) { // filled star
              scope.stars.push({
                value: i,
                filled: true
              });
            } else { // empty star
              scope.stars.push({
                value: i,
                empty: true
              });
            }
          }

          console.log(scope.stars);
        }

        calculateStars(scope.rating);

        scope.rate = function(value) {
          console.log(value);
          calculateStars(value);
          scope.rating = value;
          scope.action();
        };
      }
    };
  });
